<!-- Navigation -->
<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">Paypal for Laravel 5</a>
        </div>

        <!-- Navigation Links -->
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="{{ url('/') }}">Products</a></li>
                <li><a href="{{ url('/') }}">Sales</a></li>
                <li><a href="{{ url('/') }}">News</a></li>
                <li><a href="{{ url('/') }}">About Us</a></li>
            </ul>

            <!-- Right Side of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                @if (! Cart::isEmpty())
                    <li class="navbar-text">
                        <span class="badge">{{ Cart::getTotalQuantity() }}</span> Items &nbsp;&nbsp;
                        Total: {{ number_format(Cart::getTotal(), 2, ',', '.') }} &euro;
                    </li>
                    <li><a href="{{ url('/cart') }}">
                            <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> View Cart
                        </a>
                    </li>
                @endif

                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ url('/logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
